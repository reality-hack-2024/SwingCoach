using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepTrigger : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;

    private void OnTriggerEnter(Collider triggerObject)
    {
        // Check if the object is the one you're interested in
        if (triggerObject.CompareTag("Player"))
        {
            gameManager.EnableSwingSetup();

            Debug.Log("Player has stepped on footprints");
        }
        else
        {
            Debug.Log("Collided with wrong tag: " + triggerObject.tag);
        }
    }
}
