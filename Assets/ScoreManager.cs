using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    // 1 is best, 4 is worst
    [SerializeField] private GameObject score1;
    [SerializeField] private GameObject score2;
    [SerializeField] private GameObject score3;
    [SerializeField] private GameObject score4;

    [SerializeField] private TextMeshProUGUI scoreText;

    public void DisplayScore(float score)
    {
        score = Mathf.Round(score);
        scoreText.text = score.ToString();

        if (score < 25)
        {
            score4.SetActive(true);
        }
        else if (score >= 25 && score < 50)
        {
            score3.SetActive(true);
        }
        else if (score >= 50 && score < 75)
        {
            score2.SetActive(true);
        }
        else if (score < 75)
        {
            score1.SetActive(true);
        }
        else
        {
            scoreText.text = "0";
            Debug.LogWarning("Got unexpected score: " + score);
        }
    }

    public void ResetScore()
    {
        scoreText.text = "";

        score1.SetActive(false);
        score2.SetActive(false);
        score3.SetActive(false);
        score4.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
