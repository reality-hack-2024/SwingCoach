using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.XR;


public class SwingCircle : MonoBehaviour
{
    private Vector3 normal = Vector3.zero;

    [SerializeField] private float radius = 0.8f;
    [SerializeField] private int segments = 100;
    [SerializeField] private float angleInDegrees = 50f;
    [SerializeField] private Vector3 playerCenter;
    [SerializeField] private float playerHeight;

    [SerializeField] private ControllerManager controllerManager;
    [SerializeField] private LineRenderer circleLine;
    [SerializeField] private Vector3[] positionArray;

    void Start()
    {
        positionArray = new Vector3[segments + 1];
        normal = AngleToNormalVectorAroundX(angleInDegrees);
    }

    public void CreateSwingCircle()
    {
        circleLine.gameObject.SetActive(true);
        DrawCircle();
        RaiseCircle();
        CreateSpheres();
    }

    private void DrawCircle()
    {
        playerCenter = GetPlayerEstimates();

        circleLine.positionCount = segments + 1;
        circleLine.useWorldSpace = true;

        Vector3 v1 = Vector3.right;
        if (Vector3.Angle(normal, v1) < 0.1f) // Avoid parallel vectors
            v1 = Vector3.up;

        Vector3 v2 = Vector3.Cross(normal, v1).normalized;
        v1 = Vector3.Cross(v2, normal).normalized;

        float angle = 0f;
        for (int i = 0; i <= segments; i++)
        {
            float rad = Mathf.Deg2Rad * angle;
            Vector3 point = playerCenter + radius * (Mathf.Cos(rad) * v1 + Mathf.Sin(rad) * v2);
            positionArray[i] = point;
            angle += (360f / segments);
        }

        circleLine.SetPositions(positionArray);
    }

    private void RaiseCircle()
    {
        float lowestPartOfCircle = FindMinY(positionArray);

        Vector3 selectedControllerPos = OVRInput.GetLocalControllerPosition(controllerManager.GetSelectedTracker());

        float heightDiff = lowestPartOfCircle - selectedControllerPos.y;

        Debug.Log(heightDiff);

        for (int i = 0; i <= segments; i++)
        {
            positionArray[i].y = positionArray[i].y - heightDiff;
        }

        circleLine.SetPositions(positionArray);
    }

    public void HideSwingCircle()
    {
        circleLine.gameObject.SetActive(false);
    }


    public void CreateSpheres()
    {

    }

    public void EnableSpheres()
    {

    }

    public void DisableSpheres()
    {

    }

    Vector3 GetPlayerEstimates()
    {
        Vector3 centerEye = GameObject.Find("CenterEyeAnchor").transform.position;
        playerHeight = centerEye.y;
        radius = playerHeight / 2;
        Vector3 bodyCenter = new Vector3(centerEye.x, playerHeight / 2, centerEye.z);
        return bodyCenter;
    }

    float FindMinY(Vector3[] vectors)
    {
        float minY = float.MaxValue;

        foreach (Vector3 vec in vectors)
        {
            if (vec.y < minY)
            {
                minY = vec.y;
            }
        }

        return minY;
    }

    Vector3 AngleToNormalVectorAroundX(float angle)
    {
        float angleInRadians = angle * Mathf.Deg2Rad; // Convert to radians
        float y = Mathf.Cos(angleInRadians);
        float z = Mathf.Sin(angleInRadians);
        return new Vector3(0, y, z).normalized; // Return normalized vector
    }

}
