using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerManager : MonoBehaviour
{
    [SerializeField] private OVRInput.Controller selectedTracker;

    private void Start()
    {
        selectedTracker = OVRInput.Controller.RHand;
    }

    public OVRInput.Controller GetSelectedTracker()
    {
        return selectedTracker;
    }

    public void SetSelectedTracker(OVRInput.Controller selected)
    {
        selectedTracker = selected;
    }
}
