using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Events;

public class ControllerTracker : MonoBehaviour
{
    [SerializeField] private List<Vector3> positions;
    [SerializeField] private float maxTrackingDuration = 10.0f; // Duration in seconds
    [SerializeField] private bool isTracking = false;
    [SerializeField] private ControllerManager controllerManager;
    [SerializeField] private LineRenderer trackingLine;

    void Start()
    {
        positions = new List<Vector3>();
    }

    void Update()
    {
        if (isTracking)
        {
            // Assuming you are using the left controller for example
            Vector3 controllerPosition = OVRInput.GetLocalControllerPosition(controllerManager.GetSelectedTracker());
            Debug.Log(controllerPosition.ToString());
            positions.Add(controllerPosition);

            /*
            var temp = GameObject.CreatePrimitive(PrimitiveType.Cube);
            temp.transform.localScale = new Vector3 (0.01f, 0.01f, 0.01f);
            temp.transform.position = controllerPosition;
            */

            // Update Line Renderer
            trackingLine.positionCount = positions.Count;
            trackingLine.SetPositions(positions.ToArray());

            maxTrackingDuration -= Time.deltaTime;
            if (maxTrackingDuration <= 0)
            {
                isTracking = false;
                SaveData();
            }
        }
    }

    public void ToggleTracking()
    {
        if (isTracking)
        {
            StopTracking();
        } else
        {
            StartTracking();
        }
    }


    public void StartTracking()
    {
        trackingLine.gameObject.SetActive(true);
        isTracking = true;
        positions.Clear();
        maxTrackingDuration = 10.0f; // Reset the duration
    }

    public void StopTracking()
    {
        isTracking = false;
        SaveData();
        maxTrackingDuration = 10.0f; // Reset the duration
    }

    public void ClearTracking()
    {
        Debug.LogError("Reset");
        isTracking = false;
        positions = new List<Vector3>();
        trackingLine.SetPositions(positions.ToArray());
        trackingLine.gameObject.SetActive(false);
        maxTrackingDuration = 10.0f; // Reset the duration
    }


    private void SaveData()
    {
        string json = JsonUtility.ToJson(positions);
        //File.WriteAllText("path/to/save/data.json", json);
        Debug.Log("Data Saved");
    }
}