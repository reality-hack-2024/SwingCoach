using Meta.WitAi.Utilities;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public struct ClosestPoint
{
    public Vector3 pos;
    public float distance;
}

public class LineComparer : MonoBehaviour
{
    [SerializeField] private LineRenderer staticLine;
    [SerializeField] private LineRenderer drawnLine;
    [SerializeField] private float yThreshold = 0.1f;

    
    public float CalculateScore()
    {
        List<ClosestPoint> closestPoints = CompareLines();

        float total = 0;
        foreach (var point in closestPoints)
        {
            total += point.distance;
        }

        float score = total / closestPoints.Count();
        float normalizedScore = 7 / score;

        if (normalizedScore > 100)
        {
            normalizedScore = 100;
        }

        Debug.LogWarning("Average = " + score);
        Debug.LogWarning("Normalized Score = " + normalizedScore);

        return normalizedScore;
    }

    private List<ClosestPoint> CompareLines()
    {
        Vector3[] staticPoints = new Vector3[staticLine.positionCount];
        Vector3[] drawnPoints = new Vector3[drawnLine.positionCount];

        staticLine.GetPositions(staticPoints);
        drawnLine.GetPositions(drawnPoints);

        List<ClosestPoint> comparedPoints = new();

        for (int i = 0; i < staticPoints.Length; i++)
        {
            Vector3 closestDrawnPoint = Vector3.zero;
            float closestDistance = float.MaxValue;

            bool pointFound = false;

            foreach (var drawnPoint in drawnPoints)
            {
                if (Mathf.Abs(drawnPoint.y - staticPoints[i].y) < yThreshold)
                {
                    float distance = Vector3.Distance(staticPoints[i], drawnPoint);
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestDrawnPoint = drawnPoint;
                        pointFound = true;
                    }
                }
            }

            if (pointFound)
            {
                ClosestPoint closedPointToSwing = new ClosestPoint();
                closedPointToSwing.pos = closestDrawnPoint;
                closedPointToSwing.distance = closestDistance;
                comparedPoints.Add(closedPointToSwing);
            }

            // Here you have the closest distance for staticPoints[i]
            Debug.Log($"Closest distance to static point {i}: {closestDistance}");
        }

        return comparedPoints;
    }
}