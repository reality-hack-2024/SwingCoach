using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject welcomeScreen;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject tipsMenu;
    [SerializeField] private GameObject enableSetupMenu;
    [SerializeField] private GameObject readyMenu;
    [SerializeField] private GameObject scoreMenu;
    [SerializeField] private GameObject instructionsMenu;
    [SerializeField] private GameObject footprints;

    [SerializeField] private SwingCircle swingCircle;
    [SerializeField] private ControllerTracker controllerTracker;
    [SerializeField] private LineComparer lineComparer;
    [SerializeField] private ScoreManager scoreManager;

    [SerializeField] private float delay = 5f;

    private bool readyToStartSetup = false;
    private bool readyToTrack = false;
    private bool onFootsteps = false;

    void Start()
    {
        StartCoroutine(HideWelcome());
    }

    IEnumerator HideWelcome()
    {
        yield return new WaitForSeconds(delay);

        GoToMainMenu();
    }

    public void StartFreeStyle()
    {
        mainMenu.SetActive(false);

        instructionsMenu.SetActive(true);
        footprints.SetActive(true);
    }

    public void StartTutorial()
    {
        mainMenu.SetActive(false);

        instructionsMenu.SetActive(true);
        tipsMenu.SetActive(true);
        footprints.SetActive(true);
    }

    public void EnableSwingSetup()
    {
        if (!onFootsteps)
        {
            enableSetupMenu.SetActive(true);
            readyToStartSetup = true;
            onFootsteps = true;
        }
    }

    public void SwingCircleSetup()
    {
        if (readyToStartSetup)
        {
            readyToTrack = true;
            swingCircle.CreateSwingCircle();
            enableSetupMenu.SetActive(false);
            readyMenu.SetActive(true);
        } 
        else
        {
            Debug.LogWarning("'B' was pressed before we were ready");
        }
    }

    public void StartTracking()
    {
        if (readyToTrack)
        {
            controllerTracker.StartTracking();
            swingCircle.EnableSpheres();
        }
        else
        {
            Debug.LogWarning("Track was started before we were ready");
        }
    }

    public void StopTracking()
    {
        controllerTracker.StopTracking();
        swingCircle.DisableSpheres();

        tipsMenu.SetActive(false);
        readyMenu.SetActive(false);
        enableSetupMenu.SetActive(false);
        scoreMenu.SetActive(true);

        var normalizedScore = lineComparer.CalculateScore();
        scoreManager.DisplayScore(normalizedScore);

        readyToTrack = false;
    }

    public void ResetSwing()
    {
        readyToTrack = true;
        readyMenu.SetActive(true);
        scoreMenu.SetActive(false);
        controllerTracker.ClearTracking();
        scoreManager.ResetScore();
    }

    public void GoToMainMenu()
    {
        HideAll();
        controllerTracker.ClearTracking();
        swingCircle.HideSwingCircle();
        mainMenu.SetActive(true);
    }

    public void NextSwing()
    {
        
        ResetSwing();
    }

    private void HideAll()
    {
        welcomeScreen.SetActive(false);
        mainMenu.SetActive(false);
        tipsMenu.SetActive(false);
        scoreMenu.SetActive(false);
        instructionsMenu.SetActive(false);
        footprints.SetActive(false);

        readyToStartSetup = false;
        readyToTrack = false;
        onFootsteps = false;
    }
}
