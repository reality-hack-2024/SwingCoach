using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideMenu : MonoBehaviour
{


    public GameObject hideMenu;
    public GameObject instructionsPanel;
    public GameObject footprints;
    public GameObject modal;

    private void Start()
    {
        
    }

    public void OnClickEvent()
    {
        Debug.Log("write to console");
        hideMenu.SetActive(false);
        instructionsPanel.SetActive(true);
        footprints.SetActive(true); 
        modal.SetActive(true);


    }

    public void OnClickShowMainMenu()
    {
        hideMenu.SetActive(true);
    }
}
